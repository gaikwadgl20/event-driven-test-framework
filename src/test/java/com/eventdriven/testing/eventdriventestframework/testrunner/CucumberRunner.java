package com.eventdriven.testing.eventdriventestframework.testrunner;

import com.eventdriven.testing.eventdriventestframework.stepDefinitions.CucumberSpringConfiguration;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, features = "src/test/resources/features", plugin = {"pretty",
        "html:build/cucumber-report/cucumber-test.html",
        "json:build/cucumber-report/cucumber-test.json"}, glue ={"com.eventdriven.testing.eventdriventestframework.stepDefinitions"})
public class CucumberRunner extends CucumberSpringConfiguration {


}

