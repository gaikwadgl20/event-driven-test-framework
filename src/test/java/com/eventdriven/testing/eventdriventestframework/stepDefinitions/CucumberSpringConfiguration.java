package com.eventdriven.testing.eventdriventestframework.stepDefinitions;

import io.cucumber.java.Before;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@CucumberContextConfiguration
public class  CucumberSpringConfiguration{

}
