package com.eventdriven.testing.eventdriventestframework.stepDefinitions;

import com.eventdriven.testing.eventdriventestframework.gateway.ConsumeMessage;
import com.eventdriven.testing.eventdriventestframework.gateway.ProduceMessage;
import com.eventdriven.testing.eventdriventestframework.model.CustomerDetailsRoot;
import com.eventdriven.testing.eventdriventestframework.util.CustomerDetailsBuilder;
import com.eventdriven.testing.eventdriventestframework.util.FormatObject;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.UUID;


public class CustomerOnboardingStepDefs {

    private static final Logger logger = LoggerFactory.getLogger(CustomerOnboardingStepDefs.class);


    @Value("${kafka.topic.customer-req-topic}")
    private String customerOnboardingRequestTopic;

    @Value("${kafka.topic.customer-res-topic}")
    private String customerOnboardingResponseTopic;

    @Value("${kafka.topic.customer-err-topic}")
    private String customerOnboardingFailureTopic;

    @Value("${customer.onboarding.message}")
    private String customerOnboardingSuccessMessage;

    @Value("${customer.onboarding.failureMessage}")
    private String customerOnboardingErrorMessage;


    @Autowired
    ProduceMessage produceMessage;

    @Autowired
    ConsumeMessage consumeMessage;

    @Autowired
    FormatObject formatObject;

    @Autowired
    CustomerDetailsBuilder customerDetailsBuilder;

    String successKey;
    String firstNameFailureKey;
    String lastNameFailureKey;
    String addressFailureKey;
    String contactNumberFailureKey;
    CustomerDetailsRoot customerDetailsRoot = new CustomerDetailsRoot();

    private Scenario scenario;

    @Before
    public void before(Scenario scenario){
        this.scenario = scenario;
    }

    @Given("Success- Customer provided input for first name, last name, address, contact number.")
    public void successCustomerProvidedInputForFirstNameLastNameAddressContactNumber() {

        logger.info("Building CustomerDetails Object.");
        customerDetailsRoot = customerDetailsBuilder
                .customerDetailsBuilder("Ganesh", "Gaikwad", "Navi Mumbai", "1234567890");
        successKey = UUID.randomUUID().toString();
        logger.info("Message produced on topic: {} with key: {}."
                , customerOnboardingRequestTopic
                , successKey
        );
    }

    @When("Success- Request for customer onboarding produced succesfuly.")
    public void successRequestForCustomerOnboardingProducedSuccesfuly() {

        String produceStatus = produceMessage.produceMessage(customerOnboardingRequestTopic, successKey, customerDetailsRoot);
        logger.info("Produce Message Status: {}", produceStatus);

        logger.info("Asserting producer status.");
        Assert.assertFalse(produceStatus.equalsIgnoreCase("Failed to produce event."));

        String message=formatObject.buildJsonMessage(customerDetailsRoot);
        scenario.log("Produced Message: "+ message);

    }

    @Then("Success- Response for customer onboarding consumed successfully.")
    public void successResponseForCustomerOnboardingConsumerSuccesfuly() {
        logger.info("Consuming Messages from Kafka");

       CustomerDetailsRoot customerDetailsRootResponse = consumeMessage.consumeKafkaTopic(successKey,customerOnboardingResponseTopic);

       Assert.assertTrue(customerDetailsRootResponse
               .getCustomerDetails()
               .getMessage()
               .equalsIgnoreCase(customerOnboardingSuccessMessage));

       Assert.assertEquals(
               customerDetailsRoot.getCustomerDetails().getFirstName(),
               customerDetailsRootResponse.getCustomerDetails().getFirstName());

       Assert.assertEquals(
               customerDetailsRoot.getCustomerDetails().getLastName(),
               customerDetailsRootResponse.getCustomerDetails().getLastName());

       Assert.assertEquals(
               customerDetailsRoot.getCustomerDetails().getAddress(),
               customerDetailsRootResponse.getCustomerDetails().getAddress());

       Assert.assertEquals(
               customerDetailsRoot.getCustomerDetails().getContactNumber(),
               customerDetailsRootResponse.getCustomerDetails().getContactNumber());

        Assert.assertFalse(customerDetailsRootResponse.getCustomerDetails().getCustomerId().isEmpty());

    }

    @And("Success- Customer should be onboarded succesfully.")
    public void successCustomerShouldBeOnboardedSuccesfully() {
        logger.info("Customer onboarded successfully.");
        scenario.log("Customer onboarded successfully.");

    }

    @Given("FirstName Failure- Customer provided input for last name, address, contact number and first name is null.")
    public void firstnameFailureCustomerProvidedInputForLastNameAddressContactNumberAndFirstNameIsNull() {
        logger.info("Building CustomerDetails Object.");
        customerDetailsRoot = customerDetailsBuilder
                .customerDetailsBuilder(null, "Gaikwad", "Navi Mumbai", "1234567890");
        firstNameFailureKey = UUID.randomUUID().toString();
        logger.info("Message produced on topic: {} with key: {}."
                , customerOnboardingRequestTopic
                , firstNameFailureKey
        );
    }

    @When("FirstName Failure- Request for customer onboarding produced succesfuly.")
    public void firstnameFailureRequestForCustomerOnboardingProducedSuccesfuly() {
        String produceStatus = produceMessage.produceMessage(customerOnboardingRequestTopic, firstNameFailureKey, customerDetailsRoot);
        logger.info("Produce Message Status: {}", produceStatus);

        logger.info("Asserting producer status.");
        Assert.assertFalse(produceStatus.equalsIgnoreCase("Failed to produce event."));

        String message=formatObject.buildJsonMessage(customerDetailsRoot);
        scenario.log("Produced Message:"+ message);

    }

    @Then("FirstName Failure- Response for customer onboarding consumed successfully.")
    public void firstnameFailureResponseForCustomerOnboardingConsumerSuccesfuly() {
        logger.info("Consuming Messages from Kafka");

        CustomerDetailsRoot customerDetailsRootResponse = consumeMessage.consumeKafkaTopic(firstNameFailureKey,customerOnboardingFailureTopic);

        logger.info("customerDetailsResponse for assertions: {}",customerDetailsRootResponse);

        Assert.assertTrue(customerDetailsRootResponse
                .getCustomerDetails()
                .getMessage()
                .equalsIgnoreCase(customerOnboardingErrorMessage));

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getFirstName(),
                customerDetailsRootResponse.getCustomerDetails().getFirstName());

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getLastName(),
                customerDetailsRootResponse.getCustomerDetails().getLastName());

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getAddress(),
                customerDetailsRootResponse.getCustomerDetails().getAddress());

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getContactNumber(),
                customerDetailsRootResponse.getCustomerDetails().getContactNumber());

        Assert.assertNull(customerDetailsRootResponse.getCustomerDetails().getCustomerId());

    }

    @And("FirstName Failure- Customer onboarding should fail.")
    public void firstnameFailureCustomerOnboardingShouldFail() {
        logger.info("Customer onboarding failed as first name is empty/null.");
        scenario.log("Customer onboarding failed as first name is empty/null.");

    }

    @Given("LastName Failure- Customer provided input for first name, address, contact number and last name is null.")
    public void lastnameFailureCustomerProvidedInputForFirstNameAddressContactNumberAndLastNameIsNull() {
        logger.info("Building CustomerDetails Object.");
        customerDetailsRoot = customerDetailsBuilder
                .customerDetailsBuilder("Ganesh", null, "Navi Mumbai", "1234567890");
        lastNameFailureKey = UUID.randomUUID().toString();
        logger.info("Message produced on topic: {} with key: {}."
                , customerOnboardingRequestTopic
                , lastNameFailureKey
        );
    }

    @When("LastName Failure- Request for customer onboarding produced succesfuly.")
    public void lastnameFailureRequestForCustomerOnboardingProducedSuccesfuly() {
        String produceStatus = produceMessage.produceMessage(customerOnboardingRequestTopic, lastNameFailureKey, customerDetailsRoot);
        logger.info("Produce Message Status: {}", produceStatus);

        logger.info("Asserting producer status.");
        Assert.assertFalse(produceStatus.equalsIgnoreCase("Failed to produce event."));

        String message=formatObject.buildJsonMessage(customerDetailsRoot);
        scenario.log("Produced Message:"+ message);
    }

    @Then("LastName Failure- Response for customer onboarding consumed successfully.")
    public void lastnameFailureResponseForCustomerOnboardingConsumerSuccesfuly() {
        logger.info("Consuming Messages from Kafka");

        CustomerDetailsRoot customerDetailsRootResponse = consumeMessage.consumeKafkaTopic(lastNameFailureKey,customerOnboardingFailureTopic);

        Assert.assertTrue(customerDetailsRootResponse
                .getCustomerDetails()
                .getMessage()
                .equalsIgnoreCase(customerOnboardingErrorMessage));

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getFirstName(),
                customerDetailsRootResponse.getCustomerDetails().getFirstName());

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getLastName(),
                customerDetailsRootResponse.getCustomerDetails().getLastName());

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getAddress(),
                customerDetailsRootResponse.getCustomerDetails().getAddress());

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getContactNumber(),
                customerDetailsRootResponse.getCustomerDetails().getContactNumber());

        Assert.assertNull(customerDetailsRootResponse.getCustomerDetails().getCustomerId());

    }

    @And("LastName Failure- Customer onboarding should fail.")
    public void lastnameFailureCustomerOnboardingShouldFail() {
        logger.info("Customer onboarding failed as last name is empty/null.");
        scenario.log("Customer onboarding failed as last name is empty/null.");

    }

    @Given("Address Failure- Customer provided input for first name,last name, contact number and address is null.")
    public void addressFailureCustomerProvidedInputForFirstNameLastNameContactNumberAndAddressIsNull() {
        logger.info("Building CustomerDetails Object.");
        customerDetailsRoot = customerDetailsBuilder
                .customerDetailsBuilder("Ganesh", "Gaikwad", null, "1234567890");
        addressFailureKey = UUID.randomUUID().toString();
        logger.info("Message produced on topic: {} with key: {}."
                , customerOnboardingRequestTopic
                , addressFailureKey
        );
    }

    @When("Address Failure- Request for customer onboarding produced succesfuly.")
    public void addressFailureRequestForCustomerOnboardingProducedSuccesfuly() {
        String produceStatus = produceMessage.produceMessage(customerOnboardingRequestTopic, addressFailureKey, customerDetailsRoot);
        logger.info("Produce Message Status: {}", produceStatus);

        logger.info("Asserting producer status.");
        Assert.assertFalse(produceStatus.equalsIgnoreCase("Failed to produce event."));

        String message=formatObject.buildJsonMessage(customerDetailsRoot);
        scenario.log("Produced Message:"+ message);
    }

    @Then("Address Failure- Response for customer onboarding consumed successfully.")
    public void addressFailureResponseForCustomerOnboardingConsumerSuccesfuly() {
        logger.info("Consuming Messages from Kafka");

        CustomerDetailsRoot customerDetailsRootResponse = consumeMessage.consumeKafkaTopic(addressFailureKey,customerOnboardingFailureTopic);

        Assert.assertTrue(customerDetailsRootResponse
                .getCustomerDetails()
                .getMessage()
                .equalsIgnoreCase(customerOnboardingErrorMessage));

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getFirstName(),
                customerDetailsRootResponse.getCustomerDetails().getFirstName());

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getLastName(),
                customerDetailsRootResponse.getCustomerDetails().getLastName());

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getAddress(),
                customerDetailsRootResponse.getCustomerDetails().getAddress());

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getContactNumber(),
                customerDetailsRootResponse.getCustomerDetails().getContactNumber());

        Assert.assertNull(customerDetailsRootResponse.getCustomerDetails().getCustomerId());

    }

    @And("Address Failure- Customer onboarding should fail.")
    public void addressFailureCustomerOnboardingShouldFail() {
        logger.info("Customer onboarding failed as address is empty/null.");
        scenario.log("Customer onboarding failed as address is empty/null.");
    }

    @Given("Contact Number Failure- Customer provided input for first name,last name, address and contact number is null.")
    public void contactNumberFailureCustomerProvidedInputForFirstNameLastNameAddressAndContactNumberIsNull() {
        logger.info("Building CustomerDetails Object.");
        customerDetailsRoot = customerDetailsBuilder
                .customerDetailsBuilder("Ganesh", "Gaikwad", "Navi Mumbai", null);
        contactNumberFailureKey = UUID.randomUUID().toString();
        logger.info("Message produced on topic: {} with key: {}."
                , customerOnboardingRequestTopic
                , contactNumberFailureKey
        );
    }

    @When("Contact Number Failure- Request for customer onboarding produced succesfuly.")
    public void contactNumberFailureRequestForCustomerOnboardingProducedSuccesfuly() {
        String produceStatus = produceMessage.produceMessage(customerOnboardingRequestTopic, contactNumberFailureKey, customerDetailsRoot);
        logger.info("Produce Message Status: {}", produceStatus);

        logger.info("Asserting producer status.");
        Assert.assertFalse(produceStatus.equalsIgnoreCase("Failed to produce event."));

        String message=formatObject.buildJsonMessage(customerDetailsRoot);
        scenario.log("Produced Message:"+ message);
    }

    @Then("Contact Number Failure- Response for customer onboarding consumed successfully.")
    public void contactNumberFailureResponseForCustomerOnboardingConsumerSuccesfuly() {
        logger.info("Consuming Messages from Kafka");

        CustomerDetailsRoot customerDetailsRootResponse = consumeMessage.consumeKafkaTopic(contactNumberFailureKey,customerOnboardingFailureTopic);

        Assert.assertTrue(customerDetailsRootResponse
                .getCustomerDetails()
                .getMessage()
                .equalsIgnoreCase(customerOnboardingErrorMessage));

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getFirstName(),
                customerDetailsRootResponse.getCustomerDetails().getFirstName());

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getLastName(),
                customerDetailsRootResponse.getCustomerDetails().getLastName());

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getAddress(),
                customerDetailsRootResponse.getCustomerDetails().getAddress());

        Assert.assertEquals(
                customerDetailsRoot.getCustomerDetails().getContactNumber(),
                customerDetailsRootResponse.getCustomerDetails().getContactNumber());

        Assert.assertNull(customerDetailsRootResponse.getCustomerDetails().getCustomerId());

    }

    @And("Contact Number Failure- Customer onboarding should fail.")
    public void contactNumberFailureCustomerOnboardingShouldFail() {
        logger.info("Customer onboarding failed as contact number is empty/null.");
        scenario.log("Customer onboarding failed as contact number is empty/null.");

    }
}
