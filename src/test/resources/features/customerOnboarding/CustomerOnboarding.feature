Feature: Customer Onboarding Success Test.

  @CustomerOnBoardingSuccess
  Scenario: Customer should onboard successfully with proper inputs.
    Given Success- Customer provided input for first name, last name, address, contact number.
    When Success- Request for customer onboarding produced succesfuly.
    Then Success- Response for customer onboarding consumed successfully.
    And Success- Customer should be onboarded succesfully.

  @CustomerOnBoardingFirstNameFailure
  Scenario:  Customer should onboard should fail for improper/empty first name.
    Given FirstName Failure- Customer provided input for last name, address, contact number and first name is null.
    When FirstName Failure- Request for customer onboarding produced succesfuly.
    Then FirstName Failure- Response for customer onboarding consumed successfully.
    And FirstName Failure- Customer onboarding should fail.

  @CustomerOnBoardingLastNameFailure
  Scenario:  Customer should onboard should fail for improper/empty last name.
    Given LastName Failure- Customer provided input for first name, address, contact number and last name is null.
    When LastName Failure- Request for customer onboarding produced succesfuly.
    Then LastName Failure- Response for customer onboarding consumed successfully.
    And LastName Failure- Customer onboarding should fail.

  @CustomerOnBoardingAddressFailure
  Scenario:  Customer should onboard should fail for improper/empty address.
    Given Address Failure- Customer provided input for first name,last name, contact number and address is null.
    When Address Failure- Request for customer onboarding produced succesfuly.
    Then Address Failure- Response for customer onboarding consumed successfully.
    And Address Failure- Customer onboarding should fail.

  @CustomerOnBoardingContactNumberFailure
  Scenario:  Customer should onboard should fail for improper/empty contact.
    Given Contact Number Failure- Customer provided input for first name,last name, address and contact number is null.
    When Contact Number Failure- Request for customer onboarding produced succesfuly.
    Then Contact Number Failure- Response for customer onboarding consumed successfully.
    And Contact Number Failure- Customer onboarding should fail.


