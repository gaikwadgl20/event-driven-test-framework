package com.eventdriven.testing.eventdriventestframework.gateway;

import com.eventdriven.testing.eventdriventestframework.model.CustomerDetailsRoot;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Properties;

@Component
public class ConsumeMessage {
    private static final Logger logger = LoggerFactory.getLogger(ConsumeMessage.class);


    public CustomerDetailsRoot consumeKafkaTopic(String key, String topic) {
    Properties consumerProperties = new Properties();
        consumerProperties.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        consumerProperties.put(CommonClientConfigs.GROUP_ID_CONFIG, "test-cg-customer-onboarding");

        consumerProperties.put("enable.auto.commit", "true");
        consumerProperties.put("auto.commit.interval.ms", "1000");
        consumerProperties.put("session.timeout.ms", "30000");
        consumerProperties.put("key.deserializer",
                StringDeserializer.class.getName());
        consumerProperties.put("value.deserializer",
                StringDeserializer.class.getName());



        Consumer<String, Object> consumer = new KafkaConsumer<>(consumerProperties);
        CustomerDetailsRoot customerDetailsRoot = null;
        try {
            boolean flag = false;
            consumer.subscribe(Arrays.asList(topic));

            ConsumerRecords<String, Object> consumerRecords;

            Instant startConsumer = Instant.now();

            logger.info("Consumer Starting.");

            while (true) {
                consumerRecords = consumer.poll(Duration.ofMillis(1000));
                Instant end = Instant.now();
                if (consumerRecords != null && consumerRecords.count() > 0) {

                    logger.info("Number of Records received from Kafka: {}", consumerRecords.count());
                    for (ConsumerRecord<String, Object> record : consumerRecords) {
                        logger.info("Consumed Record: {}", record);

                        customerDetailsRoot = filterMessage(record, key);
                        if (customerDetailsRoot != null) {
                            logger.info("Setting flag for when message received");
                            logger.info("Matched Customer Details Record: {}", record.value());
                            flag = true;
                        }
                    }
                }
                Duration timeElapsed = Duration.between(startConsumer, end);
                logger.info("Consumer Running for: {} milliseconds.", timeElapsed.toMillis());

                consumer.commitSync();

                //Closing the consumer if wait time is finished.
                if (flag || timeElapsed.toMillis() > 50000) {
                    logger.info("Wait Time: {}", 50000);
                    logger.info("Consumer Flag: {}", flag);
                    logger.info("Closing the consumer.");
                    break;
                }
            }
        } catch (Exception e) {
            logger.error("Exception while consuming event: {}", e.getMessage());
        } finally {
            consumer.close();
        }
        return customerDetailsRoot;
    }


    private CustomerDetailsRoot filterMessage(ConsumerRecord<String, Object> record, String key) {
        CustomerDetailsRoot customerDetailsRoot = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        try {
            String value = record.value().toString();
            logger.info("Record received: {}", value);


            logger.info("Record key for filterring: {}", key);
            logger.info("Record key from consumed record: {}", record.key());

            if (record.key().equalsIgnoreCase(key)) {
                customerDetailsRoot = mapper.readValue(value, CustomerDetailsRoot.class);
                logger.info("Matching event: {}", customerDetailsRoot);
            } else
                logger.error("Event Mismatch");

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return customerDetailsRoot;
    }
}
