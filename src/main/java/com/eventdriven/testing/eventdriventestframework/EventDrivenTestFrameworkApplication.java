package com.eventdriven.testing.eventdriventestframework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventDrivenTestFrameworkApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventDrivenTestFrameworkApplication.class, args);
	}

}
