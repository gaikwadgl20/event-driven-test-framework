package com.eventdriven.testing.eventdriventestframework.util;

import com.eventdriven.testing.eventdriventestframework.model.CustomerDetails;
import com.eventdriven.testing.eventdriventestframework.model.CustomerDetailsRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CustomerDetailsBuilder {
    private static final Logger logger = LoggerFactory.getLogger(CustomerDetailsBuilder.class);


    @Value("${customer.onboarding.message}")
    private String customerOnboardingSuccessMessage;


    public CustomerDetailsRoot customerDetailsBuilder(String fName, String lName, String address, String contactNo){
        CustomerDetailsRoot customerDetailsRoot = new CustomerDetailsRoot();

        CustomerDetails customerDetails= new CustomerDetails();
        customerDetails.setFirstName(fName);
        customerDetails.setLastName(lName);
        customerDetails.setAddress(address);
        customerDetails.setContactNumber(contactNo);

        customerDetailsRoot.setCustomerDetails(customerDetails);
        return customerDetailsRoot;
    }

    public CustomerDetailsRoot customerDetailsResponseBuilder(String fName, String lName, String address, String contactNo){
        CustomerDetailsRoot customerDetailsRoot = new CustomerDetailsRoot();

        CustomerDetails customerDetails= new CustomerDetails();
        String uniqueID= UUID.randomUUID().toString();
        customerDetails.setCustomerId(uniqueID);
        customerDetails.setFirstName(fName);
        customerDetails.setLastName(lName);
        customerDetails.setAddress(address);
        customerDetails.setContactNumber(contactNo);
        customerDetails.setMessage(customerOnboardingSuccessMessage);

        logger.info("CustomerDetails Response Object: {}",customerDetails);
        customerDetailsRoot.setCustomerDetails(customerDetails);
        return customerDetailsRoot;
    }
}
